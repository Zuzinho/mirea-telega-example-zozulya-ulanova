package my.telegram.bot

import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.Location
import com.github.kotlintelegrambot.logging.LogLevel
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.dispatcher.location
import com.github.kotlintelegrambot.dispatcher.message
import com.github.kotlintelegrambot.entities.files.Voice
import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.utils.io.core.*
import io.ktor.utils.io.jvm.javaio.*
import java.lang.Math.*

var bot: Bot? = null

fun main() {
    val token: String = System.getenv("TG_BOT_TOKEN")

    val builder = Bot.Builder()
    builder.token = token
    builder.logLevel = LogLevel.All()
    builder.dispatch {
        message {
            if (message.voice != null) {
                handleVoice(message.voice,message.chat.id)
            }
        }
        location {
            handleLocation(message.location, message.chat.id)
        }
        command("start") {
            bot.sendMessage(chatId = ChatId.fromId(message.chat.id), text = "Bot started")
        }
    }

    bot = builder.build()
    bot?.startPolling()
}

fun handleVoice(v: Voice?, chatId: Long) {
    bot?.sendMessage(chatId = ChatId.fromId(chatId), text = "Voice message duration: ${v?.duration} seconds")
}

var locationsMap: MutableMap<Long, Location?> = mutableMapOf()

fun handleLocation(location: Location?, chatId: Long) {
    if (location != null) {
        val responseText = StringBuilder("Current location: ${location.latitude}, ${location.longitude}")
        val lastLocation: Location? = locationsMap.getOrDefault(chatId, null)
        if (lastLocation != null) {
            val distance = calculateDistance(lastLocation!!, location)
            responseText.append("\nDistance from last location: $distance meters")
        }
        locationsMap.put(chatId, location)
        bot?.sendMessage(chatId = ChatId.fromId(chatId), text = responseText.toString())
    }
}

fun calculateDistance(start: Location, end: Location): Double {
    val earthRadius = 6371e3
    val dLat = toRadians((end.latitude - start.latitude).toDouble())
    val dLon = toRadians((end.longitude - start.longitude).toDouble())
    val a = sin(dLat / 2) * sin(dLat / 2) +
            cos(toRadians(start.latitude.toDouble())) * cos(toRadians(end.latitude.toDouble())) *
            sin(dLon / 2) * sin(dLon / 2)
    val c = 2 * atan2(sqrt(a), sqrt(1 - a))
    return earthRadius * c
}
